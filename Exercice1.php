<?php


$array = [
        'Nom' => 'Bourel',
        'Adresse' => '63 rue du bidule',
        'Code Postal' => '59000',
        'Ville' => 'Lille',
        'Email' => 'amaurybourel@laposte.net',
        'Téléphone' => 'XX.XX.XX.XX.XX',
        'Birthday' => '1996-02-21',
];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Ici , l'affichage du tableau</h1>
    
    <!-- Boucle pour afficher le contenue dans une liste -->
    <?php foreach ($array as $key => $value): ?>
    <ul>
        <li>
            <?php
                if( $key == "Birthday") {
                    $date = new DateTime($value);
                    echo $key." : ". $date->format('d/m/Y');
                } else {
                    echo $key." : ". $value;
                }
            ?>
        </li>
    </ul>
    <?php endforeach; ?>


</body>
</html>
