<?php

// --------------------
// CONFIG DU PROGRAMME
// --------------------

// Adresse du serveur de base de données
// "localhost" est un alias de 127.0.0.1
$host = "127.0.0.1";

// Nom d'utilisateur de la base de données
$user = "root";

// Mot de passe associé à l'utilisateur (root) de la base de données
$pass = "";

// Nom de la base de données sur laquelle on va travailler
$database = "exo3";

// --------------------
// CONNEXION BDD
// --------------------

// On test la connexion à la BDD
try {
    // Création de la connexion à la base de données
    $pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $pass);
}
// Si la connexion échoue, on attrape l'exception (message d'erreur)
// et on arrete l'execution du programme
catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}


// --------------------
// flashbag
// --------------------
function setFlashbag($state, $message) {
    if (!isset($_SESSION['flashbag'])) {
        $_SESSION['flashbag'] = [];
    }

    array_push($_SESSION['flashbag'], [
        "state" => $state,
        "message" => $message
    ]);
}

function getFlashbag() {
    if (!empty($_SESSION['flashbag'])) {
        // Afficher le flashbag

        foreach ($_SESSION['flashbag'] as $key => $value) {
            echo "<div class=\"alert alert-".$value['state']."\">";
            echo $value['message'];
            echo "</div>";
        }

        // Suppression du message de flashbag
        unset($_SESSION['flashbag']);
    }
}
// --------------------
// Formulaire
// --------------------


// Definition des variables par défaut
$actors = null;
$title = null;
$director = null;
$producer = null;
$year_of_prod = null;
$language = null;
$category = null;
$storyline = null;
$video = null;

// On controle si l'utilisateur envois le formulaire d'inscription
if (isset($_POST['sendRegistration'])) {

    // Récupération des données du formulaire
    $actors          = isset($_POST['actors']) ? ($_POST['actors']) : null;
    $title          = isset($_POST['title']) ? ($_POST['title']) : null;
    $director        = isset($_POST['director']) ? $_POST['director'] : null;
    $producer        = isset($_POST['producer']) ? ($_POST['producer']) : null;
    $year_of_prod    = isset($_POST['year_of_prod']) ? ($_POST['year_of_prod']) : null;
    $language        = isset($_POST['language']) ? ($_POST['language']) : null;
    $category        = isset($_POST['category']) ? ($_POST['category']) : null;
    $storyline       = isset($_POST['storyline']) ? ($_POST['storyline']) : null;
    $video           = isset($_POST['video']) ? ($_POST['video']) : null;

$send = true;

// Controle des 5 caractère minimum
if (strlen($actors) < 5 ) {
    $send = false;
    setFlashbag("danger", "Le champs Actors doit contenir 5 caractères au minimum.");
}
if (strlen($title) < 5 ) {
    $send = false;
    setFlashbag("danger", "Le champs Title doit contenir 5 caractères au minimum.");
}
if (strlen($director) < 5 ) {
    $send = false;
    setFlashbag("danger", "Le champs Director doit contenir 5 caractères au minimum.");
}
if (strlen($producer) < 5 ) {
    $send = false;
    setFlashbag("danger", "Le champs Producer doit contenir 5 caractères au minimum.");
}
if (strlen($storyline) < 5 ) {
    $send = false;
    setFlashbag("danger", "Le champs Storyline doit contenir 5 caractères au minimum.");
}

// Controle de l'url
if (!filter_var($video , FILTER_VALIDATE_URL)) {
    $send = false;
    setFlashbag("danger", "Veuillez saisir une url valide.");
}



 if ($send) {
     global $pdo;
     $q = "SELECT id FROM `movies` ";
     $q = $pdo->prepare($q);
     $q->bindValue(":title", $title, PDO::PARAM_STR);
     $q->execute();
    //  if ($q->fetch(PDO::FETCH_OBJ)) {
    //   // if (true) {
    //     $send = false;
    //     setFlashbag("danger", "Un film avec le titre $title est déja enregistrer.");
    // }
    $q->closeCursor();
}


if ($send) {
    global $pdo;
    // Enregistrement de l'utilusateur
    $q = "INSERT INTO `movies` (`title`,`actors`,`director`,`producer`,`year_of_prod`,`language`,`category`,`storyline`, `video`) VALUES (:title, :actors, :director, :producer, :year_of_prod, :language, :category, :storyline, :video)";
    $q = $pdo->prepare($q);
    $q->bindValue(":title", $title, PDO::PARAM_STR);
    $q->bindValue(":actors", $actors, PDO::PARAM_STR);
    $q->bindValue(":director", $director, PDO::PARAM_STR);
    $q->bindValue(":producer", $producer, PDO::PARAM_STR);
    $q->bindValue(":year_of_prod", $year_of_prod, PDO::PARAM_STR);
    $q->bindValue(":language", $language, PDO::PARAM_STR);
    $q->bindValue(":category", $category, PDO::PARAM_STR);
    $q->bindValue(":storyline", $storyline, PDO::PARAM_STR);
    $q->bindValue(":video", $video, PDO::PARAM_STR);
    $q->execute();
    $q->closeCursor();
}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Exercice 3</title>
</head>
<body>
    <!-- Flashbag -->
    <?php getFlashbag(); ?>
    <!-- End - Flashbag -->

    <!-- Formulaire d'ajout de film -->
    <form method="post">

        <div>
            <label for="title">Title</label>
            <input type="text" id="title" name="title" value="<?php echo $title; ?>">
        </div>

        <div>
            <label for="actors">Actors</label>
            <input type="text" id="actors" name="actors" value="<?php echo $actors; ?>" >
        </div>

        <div>
            <label for="director">Director</label>
            <input type="text" id="director" name="director" value="<?php echo $director; ?>">
        </div>

        <div>
            <label for="producer">Producer</label>
            <input type="text" id="producer" name="producer" value="<?php echo $producer ; ?>">
        </div>

        <div>
            <label for="year_of_prod">Year of prod</label>

            <select name="year_of_prod">
                <option value="">Années</option>
                <?php for($i=date('Y'); $i>date('Y')-150; $i--): ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php endfor; ?>
            </select>
        </div>

        <div>
            <label for="language">Language</label>

            <select name="language">
                <option value="">Langues</option>
                <option value="">Langue A</option>
                <option value="">Langue B</option>
                <option value="">Langue C</option>
                <option value="">Langue D</option>
                <option value="">Langue E</option>
                <option value="">Langue F</option>
                <option value="">Langue G</option>
            </select>
        </div>

        <div>
            <label for="category ">Category </label>
            <select name="category ">
                <option value="">Category </option>
                <option value="">Category A</option>
                <option value="">Category B</option>
                <option value="">Category C</option>
                <option value="">Category D</option>
                <option value="">Category E</option>
                <option value="">Category F</option>
                <option value="">Category G</option>
            </select>
        </div>

        <div>
            <label for="storyline">Storyline</label>
            <input type="text" id="storyline" name="storyline" value="<?php echo $storyline  ; ?>">
        </div>

        <div>
            <label for="video">Url Video</label>
            <input type="text" id="video" name="video" value="<?php echo $video  ; ?>">
        </div>

        <button type="submit" name="sendRegistration">Send movies</button>
    </form>
</body>
</html>
