<?php
$euro = null;

if (isset($_POST['validate'])) {

    $euro = isset($_POST['euro']) ? trim($_POST['euro']) : null;

    // Fonction de conversion des euros en dollars.
    function convert($euros,$exchange)
    {
        $euros = $euros*$exchange;
        return $euros;
    }

    $convert = convert($_POST['euro'],1.1457);
}
 ?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Exercice 2</title>
    <meta name="description" content="">
  </head>
    <body>

        <h2>Convert € to $</h2>

        <form method="POST" name="convert">
            <input type="number" name="euro" id="valeur" placeholder="Entrez une valeur en euros." step="0.01" min="0">
            <input type="submit" name="validate" value="Convertir">
        </form>

        <!-- Affichage du resultat de la conversion -->
        <?php
        global $convert;
        if (isset($convert)){
        echo $euro."€ = ".$convert."$";
        }
        ?>
    </body>
</html>
